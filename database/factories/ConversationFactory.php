<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;
Use App\Models\User;

$factory->define(Conversation::class, function (Faker $faker) {

    $user = App\Models\User::orderByRaw('RAND()')->first();

    return [
        'user_id' => $user->id,
        'title' => $faker->sentence,
        'body' => $faker->text,
    ];
});
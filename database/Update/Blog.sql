-- --------------------------------------------------------
-- Host:                         localhost
-- Versión del servidor:         10.4.17-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para blog
CREATE DATABASE IF NOT EXISTS `blog` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish_ci */;
USE `blog`;

-- Volcando estructura para tabla blog.articles
CREATE TABLE IF NOT EXISTS `articles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `articles_user_id_foreign` (`user_id`),
  CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.articles: ~36 rows (aproximadamente)
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT IGNORE INTO `articles` (`id`, `user_id`, `title`, `excerpt`, `body`, `created_at`, `updated_at`) VALUES
	(1, 2, 'Earum tempore sed vitae consectetur exercitationem est.', 'Inventore aut aut molestiae cupiditate aut.', 'Aut eos sed dolor aut. Veniam optio qui voluptas quae sit minus accusamus. Ab itaque velit commodi qui soluta incidunt. Quas sed et soluta eligendi magni quaerat.', '2021-01-18 01:54:06', '2021-01-18 01:54:06'),
	(2, 2, 'Dolorum quia nihil dicta quo.', 'Non dolore tempora quia.', 'Aspernatur sint et nihil incidunt laboriosam aut reiciendis. Voluptate eum repellat quia sed recusandae soluta veniam consequuntur. Ut dolores reiciendis hic repellendus. Perferendis inventore nihil est quae mollitia occaecati.', '2021-01-18 01:54:06', '2021-01-18 01:54:06'),
	(14, 4, 'abraham', 'abraham', 'abraham', '2021-01-21 03:22:04', '2021-01-21 03:22:04'),
	(15, 5, 'juanto', 'juanito', 'juanito', '2021-01-21 03:30:20', '2021-01-21 03:30:20'),
	(20, 4, 'Estafeta', 'dsfsdfs', 'dfdsfsdf', '2021-01-23 02:14:11', '2021-01-23 02:14:11'),
	(21, 4, 'Introducción', 'os artículos acompañan siempre a un sustantivo con el que concuerdan en género (masculino, femenino) y número (singular, plural). En español, existen dos tipos de artículos: los artículos indeterminados (un, una, unos, unas) y los artículos determinados (el/los, la/las, lo).\r\n\r\nEn este apartado encontrarás una explicación clara y completa acerca de los tipos de artículos que hay en español y cómo aplicarlos correctamente. En la sección de ejercicios puedes practicar lo que has aprendido y afianzar así tus conocimientos.', 'Para llamar a un ruta debemos utilizar el helper route(), mencionado anteriormente. Vamos a colocar un par de ejemplos comenzando con un método de un controlador:', '2021-01-23 16:48:37', '2021-01-23 16:48:37'),
	(22, 4, 'Leads', 'tips laravel', 'los esdkjddjksldfhsdfjhsdflsdhfksdhfkfsdfhsljkdhsjkfhskfh', '2021-01-28 21:23:52', '2021-01-28 21:23:52'),
	(23, 4, 'Leads', 'tips laravel', 'los esdkjddjksldfhsdfjhsdflsdhfksdhfkfsdfhsljkdhsjkfhskfh', '2021-01-28 21:24:37', '2021-01-28 21:24:37'),
	(24, 4, 'fvvnv', 'vbnvn', 'vbnvnvnv', '2021-01-28 21:26:01', '2021-01-28 21:26:01'),
	(25, 4, 'sdfs', 'sdfsdf', 'sdfsd', '2021-01-28 21:29:25', '2021-01-28 21:29:25'),
	(26, 4, 'Tips Laravel', 'sdfsd', 'sdfsdfs', '2021-01-28 21:53:21', '2021-01-28 21:53:21'),
	(27, 4, 'Tips Laravel', 'sdfsd', 'sdfsdfs', '2021-01-28 21:54:06', '2021-01-28 21:54:06'),
	(28, 4, 'Tips laravel 2', 'Tips laravel', 'qwa', '2021-01-28 21:55:59', '2021-01-28 21:55:59'),
	(29, 5, 'Funciones de php', 'las funciones', 'las funciones de php', '2021-01-28 22:06:35', '2021-01-28 22:06:35'),
	(30, 4, 'Tips laravel 2', 'eclsdjhsj', 'lsdksjkdj', '2021-01-28 22:23:37', '2021-01-28 22:23:37'),
	(31, 4, '1', '1', '1', '2021-01-29 01:07:57', '2021-01-29 01:07:57'),
	(32, 4, '1', '1', '1', '2021-01-29 01:10:55', '2021-01-29 01:10:55'),
	(33, 4, '2', '2', '2', '2021-01-29 01:14:03', '2021-01-29 01:14:03'),
	(34, 4, '3', '3', '3', '2021-01-29 01:15:22', '2021-01-29 01:15:22'),
	(35, 4, '1', '1', '1', '2021-01-29 01:18:13', '2021-01-29 01:18:13'),
	(36, 4, '3', '3', '3', '2021-01-29 01:19:15', '2021-01-29 01:19:15'),
	(37, 4, '1', '1', '1', '2021-01-29 01:20:15', '2021-01-29 01:20:15'),
	(38, 4, '2', '2', '2', '2021-01-29 01:21:28', '2021-01-29 01:21:28'),
	(39, 4, '1', '1', '1', '2021-01-29 01:24:33', '2021-01-29 01:24:33'),
	(40, 4, 'drgd', 'fgdgdf', 'dfg', '2021-01-29 01:25:37', '2021-01-29 01:25:37'),
	(41, 4, 'fcb', 'dfg', 'dfg', '2021-01-29 01:26:12', '2021-01-29 01:26:12'),
	(42, 4, '1', '1', '1', '2021-01-29 01:28:33', '2021-01-29 01:28:33'),
	(43, 4, '1', '1', '1', '2021-01-29 01:29:28', '2021-01-29 01:29:28'),
	(44, 4, 'w', 'w', 'w', '2021-01-29 01:32:22', '2021-01-29 01:32:22'),
	(45, 4, 'Estafeta', 'sdsd', 'sdsd', '2021-01-30 06:11:45', '2021-01-30 06:11:45'),
	(46, 4, 'hola', 'sdd', 'sdsdsd', '2021-01-30 15:52:24', '2021-01-30 15:52:24'),
	(47, 4, 'abraham', 'abraham', 'abraham', '2021-02-13 00:47:28', '2021-02-13 00:47:28'),
	(48, 4, 'abraham', 'abrham', 'abraham', '2021-02-13 00:55:42', '2021-02-13 00:55:42'),
	(49, 4, 'abraham', 'abraham', 'abraham', '2021-02-13 00:59:04', '2021-02-13 00:59:04'),
	(50, 4, 'abraham', 'abraha,', 'abraham', '2021-02-13 01:28:12', '2021-02-13 01:28:12'),
	(51, 4, 'abraham', 'abraham', 'abraham', '2021-02-13 01:33:05', '2021-02-13 01:33:05'),
	(52, 4, 'abraham', 'abraham', 'abraham', '2021-02-13 01:34:30', '2021-02-13 01:34:30'),
	(53, 4, 'Laravel', 'Laravel articulo', 'Laravel sdsdksjdksjdksjdjksdsjkdksj', '2021-02-27 16:52:59', '2021-02-27 16:52:59');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;

-- Volcando estructura para tabla blog.article_tag
CREATE TABLE IF NOT EXISTS `article_tag` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` bigint(20) unsigned NOT NULL,
  `tag_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `article_tag_article_id_tag_id_unique` (`article_id`,`tag_id`),
  KEY `article_tag_tag_id_foreign` (`tag_id`),
  CONSTRAINT `article_tag_article_id_foreign` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`),
  CONSTRAINT `article_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.article_tag: ~46 rows (aproximadamente)
/*!40000 ALTER TABLE `article_tag` DISABLE KEYS */;
INSERT IGNORE INTO `article_tag` (`id`, `article_id`, `tag_id`, `created_at`, `updated_at`) VALUES
	(15, 14, 1, '2021-01-21 03:22:04', '2021-01-21 03:22:04'),
	(16, 14, 2, '2021-01-21 03:22:04', '2021-01-21 03:22:04'),
	(17, 15, 1, '2021-01-21 03:30:20', '2021-01-21 03:30:20'),
	(18, 15, 2, '2021-01-21 03:30:20', '2021-01-21 03:30:20'),
	(19, 15, 3, '2021-01-21 03:30:20', '2021-01-21 03:30:20'),
	(27, 20, 1, '2021-01-23 02:14:12', '2021-01-23 02:14:12'),
	(28, 20, 2, '2021-01-23 02:14:12', '2021-01-23 02:14:12'),
	(29, 21, 1, '2021-01-23 16:48:37', '2021-01-23 16:48:37'),
	(30, 21, 2, '2021-01-23 16:48:37', '2021-01-23 16:48:37'),
	(31, 22, 1, '2021-01-28 21:23:52', '2021-01-28 21:23:52'),
	(32, 22, 2, '2021-01-28 21:23:52', '2021-01-28 21:23:52'),
	(33, 23, 1, '2021-01-28 21:24:37', '2021-01-28 21:24:37'),
	(34, 23, 2, '2021-01-28 21:24:37', '2021-01-28 21:24:37'),
	(35, 24, 2, '2021-01-28 21:26:01', '2021-01-28 21:26:01'),
	(36, 25, 1, '2021-01-28 21:29:25', '2021-01-28 21:29:25'),
	(37, 26, 1, '2021-01-28 21:53:21', '2021-01-28 21:53:21'),
	(38, 26, 2, '2021-01-28 21:53:21', '2021-01-28 21:53:21'),
	(39, 27, 1, '2021-01-28 21:54:06', '2021-01-28 21:54:06'),
	(40, 27, 2, '2021-01-28 21:54:06', '2021-01-28 21:54:06'),
	(41, 28, 1, '2021-01-28 21:55:59', '2021-01-28 21:55:59'),
	(42, 28, 2, '2021-01-28 21:55:59', '2021-01-28 21:55:59'),
	(43, 29, 2, '2021-01-28 22:06:35', '2021-01-28 22:06:35'),
	(44, 30, 1, '2021-01-28 22:23:38', '2021-01-28 22:23:38'),
	(45, 31, 1, '2021-01-29 01:07:57', '2021-01-29 01:07:57'),
	(46, 32, 1, '2021-01-29 01:10:55', '2021-01-29 01:10:55'),
	(47, 33, 1, '2021-01-29 01:14:03', '2021-01-29 01:14:03'),
	(48, 34, 1, '2021-01-29 01:15:22', '2021-01-29 01:15:22'),
	(49, 35, 1, '2021-01-29 01:18:13', '2021-01-29 01:18:13'),
	(50, 36, 1, '2021-01-29 01:19:15', '2021-01-29 01:19:15'),
	(51, 37, 1, '2021-01-29 01:20:16', '2021-01-29 01:20:16'),
	(52, 38, 2, '2021-01-29 01:21:28', '2021-01-29 01:21:28'),
	(53, 39, 1, '2021-01-29 01:24:33', '2021-01-29 01:24:33'),
	(54, 40, 1, '2021-01-29 01:25:37', '2021-01-29 01:25:37'),
	(55, 41, 1, '2021-01-29 01:26:12', '2021-01-29 01:26:12'),
	(56, 42, 3, '2021-01-29 01:28:33', '2021-01-29 01:28:33'),
	(57, 43, 2, '2021-01-29 01:29:28', '2021-01-29 01:29:28'),
	(58, 44, 1, '2021-01-29 01:32:22', '2021-01-29 01:32:22'),
	(59, 45, 1, '2021-01-30 06:11:45', '2021-01-30 06:11:45'),
	(60, 46, 1, '2021-01-30 15:52:24', '2021-01-30 15:52:24'),
	(61, 47, 1, '2021-02-13 00:47:29', '2021-02-13 00:47:29'),
	(62, 47, 2, '2021-02-13 00:47:29', '2021-02-13 00:47:29'),
	(63, 48, 1, '2021-02-13 00:55:43', '2021-02-13 00:55:43'),
	(64, 48, 2, '2021-02-13 00:55:43', '2021-02-13 00:55:43'),
	(65, 49, 1, '2021-02-13 00:59:04', '2021-02-13 00:59:04'),
	(66, 49, 2, '2021-02-13 00:59:04', '2021-02-13 00:59:04'),
	(67, 49, 3, '2021-02-13 00:59:04', '2021-02-13 00:59:04'),
	(68, 50, 1, '2021-02-13 01:28:12', '2021-02-13 01:28:12'),
	(69, 50, 2, '2021-02-13 01:28:12', '2021-02-13 01:28:12'),
	(70, 51, 1, '2021-02-13 01:33:05', '2021-02-13 01:33:05'),
	(71, 51, 3, '2021-02-13 01:33:05', '2021-02-13 01:33:05'),
	(72, 52, 1, '2021-02-13 01:34:30', '2021-02-13 01:34:30'),
	(73, 52, 2, '2021-02-13 01:34:30', '2021-02-13 01:34:30'),
	(74, 53, 1, '2021-02-27 16:52:59', '2021-02-27 16:52:59'),
	(75, 53, 2, '2021-02-27 16:52:59', '2021-02-27 16:52:59'),
	(76, 53, 3, '2021-02-27 16:52:59', '2021-02-27 16:52:59');
/*!40000 ALTER TABLE `article_tag` ENABLE KEYS */;

-- Volcando estructura para tabla blog.assignments
CREATE TABLE IF NOT EXISTS `assignments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `due_data` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.assignments: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `assignments` DISABLE KEYS */;
/*!40000 ALTER TABLE `assignments` ENABLE KEYS */;

-- Volcando estructura para tabla blog.conversations
CREATE TABLE IF NOT EXISTS `conversations` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `best_reply_id` bigint(20) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.conversations: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `conversations` DISABLE KEYS */;
/*!40000 ALTER TABLE `conversations` ENABLE KEYS */;

-- Volcando estructura para tabla blog.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.failed_jobs: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Volcando estructura para tabla blog.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.migrations: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT IGNORE INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2021_01_16_170515_create_posts_table', 1),
	(5, '2021_01_16_171806_create_assignments_table', 1),
	(6, '2021_01_17_032418_create_articles_table', 1),
	(7, '2021_01_18_013648_create_tags_table', 1),
	(8, '2021_01_23_164039_create_notifications_table', 2),
	(9, '2021_01_30_042606_create_conversations', 3),
	(10, '2021_01_30_043630_create_reply', 4);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Volcando estructura para tabla blog.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.notifications: ~21 rows (aproximadamente)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT IGNORE INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
	('1403cbf8-c15c-443e-94c6-1d4dd0604803', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"1"}', NULL, '2021-01-29 01:28:35', '2021-01-29 01:28:35'),
	('368a54ce-a22d-4e13-b497-9182683b7597', 'App\\Notifications\\articlenew', 'App\\Models\\User', 5, '{"title":"Funciones de php"}', NULL, '2021-01-28 22:06:36', '2021-01-28 22:06:36'),
	('3cc077d2-c3a5-40a1-bdf6-6e631521afba', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"1"}', NULL, '2021-01-29 01:24:34', '2021-01-29 01:24:34'),
	('4be46610-18cd-4d6c-9096-068d1c74f6a5', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"2"}', NULL, '2021-01-29 01:14:05', '2021-01-29 01:14:05'),
	('4c938340-f4f9-465e-abe0-7367d68bc974', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"abraham"}', NULL, '2021-02-13 00:59:05', '2021-02-13 00:59:05'),
	('50a06a22-07fa-4e56-97d6-c5aeb6d3ae2f', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"1"}', NULL, '2021-01-29 01:20:17', '2021-01-29 01:20:17'),
	('577d3932-83e6-4a2f-87de-4bc5b4781932', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"fcb"}', NULL, '2021-01-29 01:26:13', '2021-01-29 01:26:13'),
	('610563b8-da47-4fc3-a1b9-89286563d109', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"w"}', NULL, '2021-01-29 01:32:23', '2021-01-29 01:32:23'),
	('614de44f-ba61-41f3-9cc2-8acd11b0e102', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"abraham"}', NULL, '2021-02-13 00:47:35', '2021-02-13 00:47:35'),
	('75406a5a-6599-4d34-a5f1-18f1ba9c92b3', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"1"}', NULL, '2021-01-29 01:18:14', '2021-01-29 01:18:14'),
	('7f1e98ec-b9b6-48f2-a4ca-c225002e7a6b', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"1"}', NULL, '2021-01-29 01:29:29', '2021-01-29 01:29:29'),
	('82ecb5ba-b73c-4bff-ae5a-e1fbaf7dabaf', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"2"}', NULL, '2021-01-29 01:21:29', '2021-01-29 01:21:29'),
	('a2645782-9998-48cd-a0ea-ea17384d3830', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"abraham"}', NULL, '2021-02-13 01:34:31', '2021-02-13 01:34:31'),
	('afbf27e5-f66f-4b00-8c5d-46459fe6442e', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"abraham"}', NULL, '2021-02-13 01:33:07', '2021-02-13 01:33:07'),
	('b10adfbc-3373-413e-9dee-b36480777875', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"3"}', NULL, '2021-01-29 01:15:24', '2021-01-29 01:15:24'),
	('b1eda230-d9ea-4787-a45a-1b1521e0fd91', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"1"}', NULL, '2021-01-29 01:10:57', '2021-01-29 01:10:57'),
	('bb8329a3-40a9-4ad0-a4d1-2f85f242e702', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"1"}', NULL, '2021-01-29 01:07:58', '2021-01-29 01:07:58'),
	('bbff3955-4572-42e0-9cd2-b0fb6d421495', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"Estafeta"}', NULL, '2021-01-30 06:11:47', '2021-01-30 06:11:47'),
	('c4d9e30e-b2a8-44e2-80f1-7185ca423df4', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"drgd"}', NULL, '2021-01-29 01:25:39', '2021-01-29 01:25:39'),
	('d19aec7d-663f-422c-8db7-f061732352ac', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"Tips laravel 2"}', '2021-01-28 22:23:53', '2021-01-28 22:23:39', '2021-01-28 22:23:53'),
	('dacac493-27be-44ba-adf9-a061afad33fc', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"Laravel"}', NULL, '2021-02-27 16:53:02', '2021-02-27 16:53:02'),
	('e73d0f35-9534-4d23-8742-a2930a0c61b9', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"3"}', NULL, '2021-01-29 01:19:16', '2021-01-29 01:19:16'),
	('eb74c44a-36a4-4a4b-ba82-b04edf31dff2', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"abraham"}', NULL, '2021-02-13 00:55:44', '2021-02-13 00:55:44'),
	('fe98ba2e-791b-41f1-b683-4365104d8fd4', 'App\\Notifications\\articlenew', 'App\\Models\\User', 4, '{"title":"hola"}', NULL, '2021-01-30 15:52:25', '2021-01-30 15:52:25');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Volcando estructura para tabla blog.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.password_resets: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT IGNORE INTO `password_resets` (`email`, `token`, `created_at`) VALUES
	('abrahamloranca@gmail.com', '$2y$10$WUn/Vx9i3XwCFNDW8UlxV.KlG5VeVKO.Hz6.as7zbrxkNZ8oK6edu', '2021-02-12 03:55:14');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Volcando estructura para tabla blog.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `published_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.posts: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Volcando estructura para tabla blog.reply
CREATE TABLE IF NOT EXISTS `reply` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `conversation_id` bigint(20) unsigned NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `reply_user_id_foreign` (`user_id`),
  KEY `reply_conversation_id_foreign` (`conversation_id`),
  CONSTRAINT `reply_conversation_id_foreign` FOREIGN KEY (`conversation_id`) REFERENCES `conversations` (`id`) ON DELETE CASCADE,
  CONSTRAINT `reply_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.reply: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `reply` ENABLE KEYS */;

-- Volcando estructura para tabla blog.tags
CREATE TABLE IF NOT EXISTS `tags` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.tags: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT IGNORE INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'Laravel', '2021-01-17 19:55:08', NULL),
	(2, 'PHP', '2021-01-17 19:55:19', '2021-01-17 19:55:19'),
	(3, 'Artisan', '2021-01-17 19:55:30', '2021-01-17 19:55:31');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;

-- Volcando estructura para tabla blog.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Volcando datos para la tabla blog.users: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT IGNORE INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Prof. Emilio Daniel DDS', 'ndooley@example.net', '2021-01-18 01:53:51', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '6hFDHl56aN', '2021-01-18 01:53:51', '2021-01-18 01:53:51'),
	(2, 'Mr. Richie Osinski', 'lhahn@example.com', '2021-01-18 01:53:51', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EZsS7hed2l', '2021-01-18 01:53:51', '2021-01-18 01:53:51'),
	(3, 'Mabel Moore', 'lullrich@example.net', '2021-01-18 01:53:51', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'IaSWKWbJ3Q', '2021-01-18 01:53:51', '2021-01-18 01:53:51'),
	(4, 'abraham', 'abrahamloranca@gmail.com', NULL, '$2y$10$x3y3hj3X6gse4Gvc8HAcvu1roJssvtHlbN1BoHXHYCdCDWtnaRX9m', 'm6t2dAMCV69j6uANSANE7bdI1dNpXkGIT73ku73ieqvPOpxFFy2jW5XVDQZe', '2021-01-21 01:47:21', '2021-01-21 03:44:41'),
	(5, 'juanito', 'juanito@gmail.com', NULL, '$2y$10$oZbcDQV1S1TJ2rao1RtS/uYW6abAXZgsXbEHqRnAD.SnMRKSnE0Si', '5HTPjnUArNXy1LiGcFOJR7JOEt0UMIjE5TxZZTSeTdCJSwoOZYEORuf0YVQF', '2021-01-21 03:29:30', '2021-01-28 22:06:16'),
	(6, 'juan', 'juanito123@gmail.com', NULL, '$2y$10$UJzHzOL0amTGdodKZIx1Z.CENEfBLyCoho6RvCHc4QwsSmCSFChES', NULL, '2021-01-30 15:54:39', '2021-01-30 15:54:39');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

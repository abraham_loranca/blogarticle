<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
class Conversations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\User::class, 5)->create();
        factory(\App\Models\Conversation::class, 5)->create();
        factory(App\Reply::class, 15)->create();
    }
}

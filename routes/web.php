<?php

use App\Http\Controllers\ArticlesController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    $articles=App\Article::latest()->paginate(4);
    return view('about',['articles'=>$articles]);
});
Route::get('/contact','App\Http\Controllers\ContactController@show');
Route::post('/contact','App\Http\Controllers\ContactController@store');
Route::get('/notifications','App\Http\Controllers\UserNotificationsController@show')->middleware('auth');
Auth::routes();

Route::get('/conversations','App\Http\Controllers\ConversationsController@index');
Route::get('/conversations/{conversation}','App\Http\Controllers\ConversationsController@show');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/articles','App\Http\Controllers\ArticlesController@index')->name('articles.index');
Route::post('/articles','App\Http\Controllers\ArticlesController@store');    
Route::get('/articles/create','App\Http\Controllers\ArticlesController@create')->name('articles.create');
Route::get('/articles/{article}','App\Http\Controllers\ArticlesController@show')->name('articles.show');
Route::get('/articles/{article}/edit','App\Http\Controllers\ArticlesController@edit');
Route::put('/articles/{article}','App\Http\Controllers\ArticlesController@update');


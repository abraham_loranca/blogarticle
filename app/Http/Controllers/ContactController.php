<?php

namespace App\Http\Controllers;

use App\Mail\ContaactMe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;


class ContactController extends Controller
{
    public function show()
    {
        return view('contact.send');
    }
    public function store()
    {
        //send mail
        request()->validate(['email'=>'required|email']);
        
        Mail::to(request('email'))
           ->send(new ContaactMe());

        return redirect('/contact')
                        ->with('message','Email sent!');
        
    }
}

<?php

namespace App\Http\Controllers;

use App\Article;
use App\Notifications\articlenew;
use Illuminate\Http\Request;
use App\Tag;

use Notification;
use phpDocumentor\Reflection\Types\This;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller
{
   
   public function index()
   {    
        if(request('user'))
        {
            $article=Article::where('user_id',Auth::user()->id)->get();
            
        }
       elseif(request('tag')){
           $article=Tag::where('name',request('tag'))->firstorFail()->articles;
       }else{

        $article= Article::latest()->get();

       }
       return view('articles.index',['articles'=>$article]);
   }
   
    public function show(Article $article)
    {
        return view('articles.show',['article'=>$article]);
    }
    
    public function create()
    {
 
        return view('articles.create',[
            'tags'=>Tag::all()
        ]);
    }
    public function store()
    {
       
        $this->validateArticle();
        $article =new Article(request(['title','excerpt','body']));
        $article->user_id= Auth::user()->id;
        $article->save();   
        $article->tags()->attach(request('tags'));
        Notification::send(request()->user(), new articlenew(request('title')));
        return redirect(route('articles.index'));
    }
    public function edit(Article $article)
    {
        return view('articles.edit',compact('article'));
    }
    public function update(Article $article)
    {
      $article->update($this->validateArticle());
        return redirect($article->path()); 
    }
    public function validateArticle()
    {
        return request()->validate([
            'title'=>'required',
            'excerpt'=>'required',
            'body'=>'required',
            'tags'=>'exists:tags,id'
        ]);
    }
}

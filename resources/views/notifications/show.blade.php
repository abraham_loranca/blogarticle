@extends('layout')

@section('content')
<div id="wrapper">
	<div id="page" class="container">
		<div id="content">
			<div class="title">
				<h2>Notificaciones del sitio</h2>
				
             </div>
			
			<ul class="style1">
				@forelse($notifications as $notification)
                <li>
                <p>Articulos publicados recientemente:{{$notification->data['title']}}</p>
                </li>
                @empty
                <li>
                <p>No tienes Nuevas notificaciones</p>
                </li>
				@endforelse
			</ul></div>
		<div id="sidebar">
			
		</div>
	</div>
</div>
@endsection
@extends('layout')

@section('content')
<div id="wrapper">
	<div id="page" class="container">
		<div id="content">
			<div class="title">
                @forelse($articles as $article)
                        <h2><a href="{{$article->path()}}">{{$article->title}}</a></h2>
                    <p><img src="images/banner.jpg" alt="" class="image image-full" /> </p>
                    <p>{{$article->excerpt}}</p>
                    @empty
                    <p>no relevant articles yet.</p>
                @endforelse
            </div>
	    </div>
    </div>
</div>  
@endsection
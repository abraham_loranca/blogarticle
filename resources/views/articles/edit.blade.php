@extends('layout') @section('content')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
@endsection

<div id="wrapper">
    <div id="page" class="container">
        <h1 class="heading has-text-weight-bold is-size-4">Udate Article</h1>
        <form method="POST" action="/articles/{{$article->id}}">
        @csrf
        @method('PUT')
            <div class="field">
                <label for="">Title</label>
                <div class="control">
                    <input class="input" type="text" name="title" id="title" value="{{$article->title}}">
                    @error('title')
                    <p class="help is-danger">{{$errors->first('title')}}</p>
                    @enderror
                </div>
            </div>
            <div class="field">
                <label for="excerpt">excerpt</label>
                <div class="control">
                    <textarea class="textarea" id="excerpt" name="excerpt" rows="3">{{$article->excerpt}}</textarea>
                      @error('excerpt')
                    <p class="help is-danger">{{$errors->first('excerpt')}}</p>
                    @enderror
                </div>
            </div>
            <div class="field">
                <label for="body">Body</label>
                <div class="control">
                    <textarea class="textarea" id="body" name="body" name="" rows="3">{{$article->body}}</textarea>
                    @error('body')
                    <p class="help is-danger">{{$errors->first('body')}}</p>
                    @enderror
                </div>
            </div>
            <div class="field is_grouped">
                <div class="control">
                    <button class="button is-link">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
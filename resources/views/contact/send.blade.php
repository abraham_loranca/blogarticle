@extends('layout') @section('content')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.css">
@endsection

<div id="wrapper">
    <div id="page" class="container">
        <h1 class="heading has-text-weight-bold is-size-4 ">Contact</h1>
        <form method="POST" action="/contact">
        @csrf
        <div class="box ">
        <div class="column is-half is-offset-one-quarter">
        <div class="field ">
                <label class="label" for="">Correo</label>
                <div class="control">
                    <input class="input is-primary @error('email') is-danger @enderror"
                     type="text"
                     name="email" 
                     id="email" value="{{old('email')}}">
                    @error('email')
                    <p class="help is-danger">{{$errors->first('email')}}</p>
                    @enderror
                </div>
                <button type="submit" class="button is-black">Send</button>
                @if(session('message'))
                <div class="container">
                    {{session('message')}}
                </div>
                @endif
            </div>    
        </div>
    
        </div>
        
        </form>
    </div>
</div>
@endsection
@extends('layout') @section('content')


<div id="wrapper">
    <div id="page" class="container">
        <h1 class="heading has-text-weight-bold is-size-4 ">index conversations</h1>
       @foreach($conversations as $conversation)
       <h2><a href="/conversations/{{$conversation->id}}">{{$conversation->title}}</a></h2>
       @continue($loop->last)

       <hr>
       @endforeach
    </div>
</div>
@endsection
@extends('layout') @section('content')


<div id="wrapper">
    <div id="page" class="container">
        <h1 class="heading has-text-weight-bold is-size-4 ">Show conversations</h1>
      <p>
      <a href="/conversations">back</a>
      </p>

      <h1>{{$conversation->title}}</h1>

      <p class="text-muted">Posted by {{$conversation->user->name}}</p>

      <div>
      {{$conversation->body}}
      </div>

      <hr>

      @include('conversation.replies')
</div>
@endsection